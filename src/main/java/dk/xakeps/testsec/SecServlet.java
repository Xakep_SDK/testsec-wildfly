package dk.xakeps.testsec;

import javax.annotation.security.DeclareRoles;
import javax.security.enterprise.authentication.mechanism.http.BasicAuthenticationMechanismDefinition;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@BasicAuthenticationMechanismDefinition
@ServletSecurity(@HttpConstraint(rolesAllowed = "ROLE_USER"))
@DeclareRoles({"ROLE_USER", "ROLE_TEST"})
@WebServlet(name = "SecServlet", urlPatterns = "/sec", loadOnStartup = 1)
public class SecServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().write("Hello");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        if (name == null) name = "World";
        req.setAttribute("user", name);
        req.getRequestDispatcher("response.jsp").forward(req, resp);
    }
}
