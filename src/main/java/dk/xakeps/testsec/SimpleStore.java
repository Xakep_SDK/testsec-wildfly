package dk.xakeps.testsec;

import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@ApplicationScoped
public class SimpleStore implements IdentityStore {
    @Override
    public CredentialValidationResult validate(Credential credential) {
        if (credential instanceof UsernamePasswordCredential) {
            return new CredentialValidationResult(((UsernamePasswordCredential) credential).getCaller(), new HashSet<>(Arrays.asList("ROLE_USER", "ROLE_TEST")));
        }
        return CredentialValidationResult.NOT_VALIDATED_RESULT;
    }

    @Override
    public Set<String> getCallerGroups(CredentialValidationResult validationResult) {
        return new HashSet<>(Arrays.asList("ROLE_USER", "ROLE_TEST"));
    }

    @Override
    public int priority() {
        return 0;
    }

    @Override
    public Set<ValidationType> validationTypes() {
        return DEFAULT_VALIDATION_TYPES;
    }
}
